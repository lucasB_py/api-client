import { DOCUMENT } from '@angular/common';
import { Component } from '@angular/core';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent {

  searchStr: string = '';
  song: Array<any> = []; 
  
  constructor( private api: ApiService ) { }

  ngOnInit(): void {
  }

  searchMusic(){
    this.api.getSong(this.searchStr).subscribe( body => {
      this.song = body.message.body.track_list;
      console.log(this.song);
    });
  }
}
