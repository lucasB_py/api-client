import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { FormGroup, FormControl, FormArray, FormBuilder } from '@angular/forms';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [ApiService]
})


export class HomeComponent implements OnInit {
  
  /* topSongs = {
    track_name: '',
    album_name: '',
    artist_name: ''
  } */
  topSongs: Array<any> = [];

  constructor( private dataAPI: ApiService ) { }

  ngOnInit(): void {
    this.readSongs();
    /* this.dataAPI.readTop().subscribe( (res: any[]) => {
      console.log(res);
      this.topSongs = res;
    }); */  
  }

  readSongs(): void {
    this.dataAPI.readTop()
    .subscribe(
      (body: any) => {
        this.topSongs = body.message.body.track_list;
        console.log(this.topSongs);
      },
      error => {
        console.log(error);
      });
  }
}
