import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';

const routes: Routes = [
  {
    path: 'home',
    //component: HomeComponent
    loadChildren: () => import('./pages/home/home-routing.module').then( m => m.HomeRoutingModule )
  },
  {
    path: 'posts',
    loadChildren: () => import('./pages/posts/posts-routing.module').then( m => m.PostsRoutingModule )
  },
  {
    path: '**',
    redirectTo: 'home'
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
