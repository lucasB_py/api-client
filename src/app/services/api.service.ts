import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { catchError } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

//const opts = { params: new HttpParams( {fromString: 'apikey=c3738b9393831235d426cce6cbabd65c&page=1&page_size=12&country=MX&chart_name=top&format=json'}) }; */

@Injectable({
  providedIn: 'root'
})

export class ApiService {

  private baseURL = 'https://api.musixmatch.com/ws/1.1';

  constructor( private http: HttpClient ) { }

  readTop(): Observable<any> {
    return this.http.get<any>('https://cors-anywhere.herokuapp.com/https://api.musixmatch.com/ws/1.1/chart.tracks.get?apikey=7b4d0e163e4acb8c7727c7e39ec1600e&page=1&page_size=50&country=MX&chart_name=top&format=json');
  }

  getSong(str: string): Observable<any> {
    return this.http.get<any>(`https://cors-anywhere.herokuapp.com/${this.baseURL}/track.search?apikey=7b4d0e163e4acb8c7727c7e39ec1600e&q=${str}&page_size=50&page=1&format=json`);
  }

  /* public getTopSongs(){
    let params = new HttpParams();
    params = params.append('apikey', 'c3738b9393831235d426cce6cbabd65c');
    params = params.append('page', '1');
    params = params.append('page_size', '12');
    params = params.append('country', 'MX');
    params = params.append('chart_name', 'top');
    
    return this.http.get(baseURL, opts);
  } */
}
